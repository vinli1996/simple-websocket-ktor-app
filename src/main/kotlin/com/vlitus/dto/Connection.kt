package com.vlitus.dto

import io.ktor.server.websocket.*
import java.util.concurrent.atomic.AtomicInteger

class Connection(val session: WebSocketServerSession) {
    companion object {
        val lastId = AtomicInteger(0)
    }

    val name = "user-${lastId.getAndIncrement()}"


}